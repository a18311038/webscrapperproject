# WebScrapperProject

**XAMPP**


- Paso 1: Descargar XAMPP

Ve al sitio web oficial de XAMPP en https://www.apachefriends.org/es/index.html.
Selecciona la versión de XAMPP adecuada para tu sistema operativo (Windows, macOS, Linux).
Haz clic en el botón de descarga para comenzar a descargar el instalador.

- Paso 2: Ejecutar el Instalador

Una vez que se haya descargado el archivo de instalación, ábrelo haciendo doble clic en él.
Si estás en Windows, es posible que se te pida permiso para ejecutar el instalador. Haz clic en "Sí" para permitirlo.
En el proceso de instalación, es posible que se te pregunte si deseas instalar componentes adicionales como Apache, MySQL, PHP, etc. Asegúrate de seleccionar todos los componentes necesarios para tu proyecto.


- Paso 3: Configurar XAMPP
Durante la instalación, se te pedirá que elijas el directorio de instalación de XAMPP. Puedes optar por la ubicación predeterminada o elegir otra que prefieras.
Después de seleccionar la ubicación, el instalador comenzará a copiar los archivos necesarios. Esto puede llevar unos minutos, dependiendo de la velocidad de tu computadora.

- Paso 4: Iniciar XAMPP y Configurar Servicios

Una vez que la instalación haya finalizado, abre XAMPP desde el menú de inicio (en Windows) o desde la carpeta de aplicaciones (en macOS).
Dentro de la interfaz de XAMPP, verás una lista de servicios como Apache, MySQL, FileZilla, etc.
Para iniciar un servicio, haz clic en el botón "Start" junto al nombre del servicio. Espera hasta que el indicador junto al servicio se vuelva verde, lo que indica que el servicio se ha iniciado correctamente.

- Paso 5: Verificar la Instalación

Abre tu navegador web y escribe "localhost" en la barra de direcciones.
Deberías ver la página de inicio de XAMPP, lo que significa que la instalación ha sido exitosa.
Para verificar que Apache y MySQL estén funcionando correctamente, puedes hacer clic en los enlaces correspondientes en la página de inicio de XAMPP para acceder a sus respectivos tableros de control.

**Composer**

- Paso 1: Verificar los Requisitos del Sistema

Antes de instalar Composer, asegúrate de que tu sistema cumple con los siguientes requisitos:
PHP instalado (versión 5.3.2 o superior)
Acceso a Internet para descargar Composer

- Paso 2: Descargar Composer

Abre una terminal o línea de comandos en tu sistema.
Utiliza el siguiente comando para descargar el instalador de Composer: php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

- Paso 3: Instalar Composer

Una vez que se haya verificado la integridad del instalador, ejecuta el siguiente comando para instalar Composer: php composer-setup.php --install-dir=/usr/local/bin --filename=composer
Este comando instalará Composer globalmente en tu sistema en el directorio /usr/local/bin y lo renombrará a composer.

- Paso 4: Verificar la Instalación

Después de instalar Composer, verifica que se haya instalado correctamente ejecutando el siguiente comando: composer --version
Deberías ver la versión de Composer que se ha instalado, lo que confirma que la instalación fue exitosa.


**Laravel**

- Paso 1: Requisitos Previos

Antes de instalar Laravel, asegúrate de tener lo siguiente:
PHP (versión 7.3 o superior)
Composer

- Paso 2: Instalar Laravel

Abre una terminal o línea de comandos en tu sistema.
Ejecuta el siguiente comando de Composer para instalar Laravel globalmente: composer global require laravel/installer

- Paso 3: Verificar la Instalación

Después de instalar Laravel, verifica que se haya instalado correctamente ejecutando el siguiente comando: laravel --version

